//
//  main.m
//  2048
//
//  Created by Ryan Simpson on 2/9/17.
//  Copyright © 2017 Ryan Simpson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
