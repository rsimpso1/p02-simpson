//
//  SecondViewController.h
//  2048
//
//  Created by Ryan Simpson on 2/16/17.
//  Copyright © 2017 Ryan Simpson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewControllerA.h"

@interface SecondViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *L2048;
@property (weak, nonatomic) IBOutlet UILabel *Lwelcome;
@property (weak, nonatomic) IBOutlet UILabel *Lv2;
@property (weak, nonatomic) IBOutlet UIButton *pushToStart;
@property (nonatomic, strong) ViewControllerA *viewControllerA;

@end
