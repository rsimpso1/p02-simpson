//
//  outterView.m
//  2048
//
//  Created by Ryan Simpson on 2/20/17.
//  Copyright © 2017 Ryan Simpson. All rights reserved.
//

#import "outterView.h"

@implementation outterView


int highest = 2;
int totalScore = 0;
int highestScore = 0;
int tiles = 1;
- (UIColor*)color0 {
    return [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
}

- (UIColor*)color2 {
    return [UIColor colorWithRed:128.0f/255.0f green:0.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}

- (UIColor*)color4 {
    return [UIColor colorWithRed:255.0f/255.0f green:0.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}

- (UIColor*)color8 {
    return [UIColor colorWithRed:255.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
}

- (UIColor*)color16 {
    return [UIColor colorWithRed:255.0f/255.0f green:128.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
}

- (UIColor*)color32 {
    return [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
}

- (UIColor*)color64 {
    return [UIColor colorWithRed:128.0f/255.0f green:255.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
}

- (UIColor*)color128 {
    return [UIColor colorWithRed:0.0f/255.0f green:255.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
}

- (UIColor*)color256 {
    return [UIColor colorWithRed:0.0f/255.0f green:255.0f/255.0f blue:128.0f/255.0f alpha:1.0f];
}

- (UIColor*)color512 {
    return [UIColor colorWithRed:0.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}

- (UIColor*)color1024 {
    return [UIColor colorWithRed:64.0f/255.0f green:128.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}

- (UIColor*)color2048 {
    return [UIColor colorWithRed:64.0f/255.0f green:0.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}

- (UIColor*)color4096 {
    return [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}


-(void)colorTiles{
    for(int i =0;i<16;i++){
        UILabel *temp = [_all objectAtIndex:i];
        if([temp.text isEqualToString:@""]){
            temp.font=[temp.font fontWithSize:40];
            temp.textColor = [self color0];
            temp.backgroundColor = [self color0];
        }
        else if([temp.text isEqualToString:@"2"]){
            temp.font=[temp.font fontWithSize:40];
            temp.textColor = [self color0];
            temp.backgroundColor = [self color2];
        }
        else if([temp.text isEqualToString:@"4"]){
            temp.font=[temp.font fontWithSize:40];
            temp.backgroundColor = [self color4];
        }
        else if([temp.text isEqualToString:@"8"]){
            temp.font=[temp.font fontWithSize:40];
            temp.backgroundColor = [self color8];
        }
        else if([temp.text isEqualToString:@"16"]){
            temp.font=[temp.font fontWithSize:40];
            temp.backgroundColor = [self color16];
        }
        else if([temp.text isEqualToString:@"32"]){
            temp.font=[temp.font fontWithSize:40];
            temp.backgroundColor = [self color32];
        }
        else if([temp.text isEqualToString:@"64"]){
            temp.font=[temp.font fontWithSize:40];
            temp.backgroundColor = [self color64];
        }
        else if([temp.text isEqualToString:@"128"]){
            temp.font=[temp.font fontWithSize:30];
            temp.backgroundColor = [self color128];
        }
        else if([temp.text isEqualToString:@"256"]){
            temp.font=[temp.font fontWithSize:30];
            temp.backgroundColor = [self color256];
        }
        else if([temp.text isEqualToString:@"512"]){
            temp.font=[temp.font fontWithSize:30];
            temp.backgroundColor = [self color512];
        }
        else if([temp.text isEqualToString:@"1024"]){
            temp.font=[temp.font fontWithSize:24];
            temp.backgroundColor = [self color1024];
        }
        else if([temp.text isEqualToString:@"2048"]){
            temp.font=[temp.font fontWithSize:24];
            temp.backgroundColor = [self color2048];
        }
        else{
            temp.font=[temp.font fontWithSize:20];
            temp.backgroundColor = [self color4096];
        }
    }
}

-(void)colorBackground{
    if(highest == 2){
        self.backgroundColor = [self color2];
        _arrowView.backgroundColor = [self color2];
        _upButton.backgroundColor = [self color2];
        _rightButton.backgroundColor = [self color2];
        _leftButton.backgroundColor = [self color2];
        _downButton.backgroundColor = [self color2];
    }
    else if(highest == 4){
        self.backgroundColor = [self color4];
        _arrowView.backgroundColor = [self color4];
        _upButton.backgroundColor = [self color4];
        _rightButton.backgroundColor = [self color4];
        _leftButton.backgroundColor = [self color4];
        _downButton.backgroundColor = [self color4];
    }
    else if(highest == 8){
        self.backgroundColor = [self color8];
        _arrowView.backgroundColor = [self color8];
        _upButton.backgroundColor = [self color8];
        _rightButton.backgroundColor = [self color8];
        _leftButton.backgroundColor = [self color8];
        _downButton.backgroundColor = [self color8];
    }
    else if(highest == 16){
        self.backgroundColor = [self color16];
        _arrowView.backgroundColor = [self color16];
        _upButton.backgroundColor = [self color16];
        _rightButton.backgroundColor = [self color16];
        _leftButton.backgroundColor = [self color16];
        _downButton.backgroundColor = [self color16];
    }
    else if(highest == 32){
        self.backgroundColor = [self color32];
        _arrowView.backgroundColor = [self color32];
        _upButton.backgroundColor = [self color32];
        _rightButton.backgroundColor = [self color32];
        _leftButton.backgroundColor = [self color32];
        _downButton.backgroundColor = [self color32];
    }
    else if(highest == 64){
        self.backgroundColor = [self color64];
        _arrowView.backgroundColor = [self color64];
        _upButton.backgroundColor = [self color64];
        _rightButton.backgroundColor = [self color64];
        _leftButton.backgroundColor = [self color64];
        _downButton.backgroundColor = [self color64];
    }
    else if(highest == 128){
        self.backgroundColor = [self color128];
        _arrowView.backgroundColor = [self color128];
        _upButton.backgroundColor = [self color128];
        _rightButton.backgroundColor = [self color128];
        _leftButton.backgroundColor = [self color128];
        _downButton.backgroundColor = [self color128];
    }
    else if(highest == 256){
        self.backgroundColor = [self color256];
        _arrowView.backgroundColor = [self color256];
        _upButton.backgroundColor = [self color256];
        _rightButton.backgroundColor = [self color256];
        _leftButton.backgroundColor = [self color256];
        _downButton.backgroundColor = [self color256];
    }
    else if(highest == 512){
        self.backgroundColor = [self color512];
        _arrowView.backgroundColor = [self color512];
        _upButton.backgroundColor = [self color512];
        _rightButton.backgroundColor = [self color512];
        _leftButton.backgroundColor = [self color512];
        _downButton.backgroundColor = [self color512];
    }
    else if(highest == 1024){
        self.backgroundColor = [self color1024];
        _arrowView.backgroundColor = [self color1024];
        _upButton.backgroundColor = [self color1024];
        _rightButton.backgroundColor = [self color1024];
        _leftButton.backgroundColor = [self color1024];
        _downButton.backgroundColor = [self color1024];
    }
    else if(highest == 2048){
        self.backgroundColor = [self color2048];
        _arrowView.backgroundColor = [self color2048];
        _upButton.backgroundColor = [self color2048];
        _rightButton.backgroundColor = [self color2048];
        _leftButton.backgroundColor = [self color2048];
        _downButton.backgroundColor = [self color2048];
    }
    else if(highest>2048){
        self.backgroundColor = [self color4096];
        _arrowView.backgroundColor = [self color4096];
        _upButton.backgroundColor = [self color4096];
        _rightButton.backgroundColor = [self color4096];
        _leftButton.backgroundColor = [self color4096];
        _downButton.backgroundColor = [self color4096];
    }
}

-(void)updateScore{
    _scoreLabel.text = [NSString stringWithFormat:@"Score: %d", totalScore];
    if(totalScore > highestScore){
        highestScore = totalScore;
        _highScoreLabel.text = [NSString stringWithFormat:@"High Score: %d", highestScore];
    }
}

-(void)checkFull{
    int full = 1;
    int i;
    for(i=0;i<16;i++){
        UILabel *temp = [_all objectAtIndex:i];
        if([temp.text isEqualToString:@""]){
            full = 0;
        }
    }
    if(full == 1){
        NSLog(@"here");
        
        int i;
        for(i=0;i<16;i++){
            UILabel *temp = [_all objectAtIndex:i];
            if(i == 3){
                temp.text = @"2";
            }
            else
                temp.text = @"";
        }
        if(totalScore > highestScore){
            highestScore = totalScore;
            _highScoreLabel.text = [NSString stringWithFormat:@"High Score: %d", highestScore];
        }
        tiles = 1;
        totalScore = 0;
        highest = 2;
    }
    [self colorTiles];
    [self colorBackground];
    [self updateScore];
}

-(void)addElement{
    int added = 0;
    NSArray *options = [NSArray arrayWithObjects: @"2",@"4",@"8",nil];
    while(added == 0 && tiles<16){
        int x;
        int r = arc4random_uniform(15);
        if(highest>=8){
            x = arc4random_uniform(2);
        }
        else if(highest == 4){
            x = arc4random_uniform(1);
        }
        else{
            x = 0;
        }
        UILabel *rL = [_all objectAtIndex:r];
        
        
        
        //THIS NEEDS AN ELSE STATEMENT
        if([rL.text isEqualToString:@""]){
            rL.text = options[x];
            added = 1;
            tiles+=1;
        }
    }
    [self colorTiles];
    [self colorBackground];
    [self updateScore];
}

- (IBAction)handleUpClick:(id)sender {
    int i;
    int changed = 0;
    int combineThresh = -1;
    for (i = 0; i<3; i++) {
        UILabel *current = [_column1 objectAtIndex:i];
        UILabel *next = [_column1 objectAtIndex:i+1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i>0)
                i=i-2;
            else
                i--;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh<i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i--;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = -1;
    for (i = 0; i<3; i++) {
        UILabel *current = [_column2 objectAtIndex:i];
        UILabel *next = [_column2 objectAtIndex:i+1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i>0)
                i=i-2;
            else
                i--;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh<i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i--;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = -1;
    for (i = 0; i<3; i++) {
        UILabel *current = [_column3 objectAtIndex:i];
        UILabel *next = [_column3 objectAtIndex:i+1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i>0)
                i=i-2;
            else
                i--;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh<i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i--;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = -1;
    for (i = 0; i<3; i++) {
        UILabel *current = [_column4 objectAtIndex:i];
        UILabel *next = [_column4 objectAtIndex:i+1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i>0)
                i=i-2;
            else
                i--;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh<i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i--;
            changed = 1;
            tiles -=1;
        }
    }
    if(changed == 1)
        [self addElement];
    if(changed == 0)
        [self checkFull];
    
}
- (IBAction)handleRightClick:(id)sender {
    int i;
    int combineThresh = 4;
    int changed = 0;
    for (i = 3; i>0; i--) {
        UILabel *current = [_row1 objectAtIndex:i];
        UILabel *next = [_row1 objectAtIndex:i-1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i<3)
                i=i+2;
            else
                i++;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh>i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i++;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = 4;
    for (i = 3; i>0; i--) {
        UILabel *current = [_row2 objectAtIndex:i];
        UILabel *next = [_row2 objectAtIndex:i-1];
        //printf("entered row2 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i<3)
                i=i+2;
            else
                i++;
            changed = 1;
        }
        //?second part of if
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh>i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i++;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = 4;
    for (i = 3; i>0; i--) {
        UILabel *current = [_row3 objectAtIndex:i];
        UILabel *next = [_row3 objectAtIndex:i-1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i<3)
                i=i+2;
            else
                i++;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh>i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i++;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = 4;
    for (i = 3; i>0; i--) {
        UILabel *current = [_row4 objectAtIndex:i];
        UILabel *next = [_row4 objectAtIndex:i-1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i<3)
                i=i+2;
            else
                i++;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh>i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i++;
            changed = 1;
            tiles -=1;
        }
    }
    if(changed == 1)
        [self addElement];
    if(changed == 0)
        [self checkFull];
}
- (IBAction)handleLeftClick:(id)sender {
    int i;
    int combineThresh = -1;
    int changed = 0;
    for (i = 0; i<3; i++) {
        UILabel *current = [_row1 objectAtIndex:i];
        UILabel *next = [_row1 objectAtIndex:i+1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i>0)
                i=i-2;
            else
                i--;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh<i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i--;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = -1;
    for (i = 0; i<3; i++) {
        UILabel *current = [_row2 objectAtIndex:i];
        UILabel *next = [_row2 objectAtIndex:i+1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i>0)
                i=i-2;
            else
                i--;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh<i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i--;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = -1;
    for (i = 0; i<3; i++) {
        UILabel *current = [_row3 objectAtIndex:i];
        UILabel *next = [_row3 objectAtIndex:i+1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i>0)
                i=i-2;
            else
                i--;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh<i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i--;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = -1;
    for (i = 0; i<3; i++) {
        UILabel *current = [_row4 objectAtIndex:i];
        UILabel *next = [_row4 objectAtIndex:i+1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i>0)
                i=i-2;
            else
                i--;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh<i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i--;
            changed = 1;
            tiles -=1;
        }
    }
    if(changed == 1)
        [self addElement];
    if(changed == 0)
        [self checkFull];
}
- (IBAction)handleDownClick:(id)sender {
    int i;
    int combineThresh = 4;
    int changed = 0;
    for (i = 3; i>0; i--) {
        UILabel *current = [_column1 objectAtIndex:i];
        UILabel *next = [_column1 objectAtIndex:i-1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i<3)
                i=i+2;
            else
                i++;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh>i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i++;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = 4;
    for (i = 3; i>0; i--) {
        UILabel *current = [_column2 objectAtIndex:i];
        UILabel *next = [_column2 objectAtIndex:i-1];
        //printf("entered row2 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i<3)
                i=i+2;
            else
                i++;
            changed = 1;
        }
        //?second part of if
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh>i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i++;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = 4;
    for (i = 3; i>0; i--) {
        UILabel *current = [_column3 objectAtIndex:i];
        UILabel *next = [_column3 objectAtIndex:i-1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i<3)
                i=i+2;
            else
                i++;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh>i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i++;
            changed = 1;
            tiles -=1;
        }
    }
    combineThresh = 4;
    for (i = 3; i>0; i--) {
        UILabel *current = [_column4 objectAtIndex:i];
        UILabel *next = [_column4 objectAtIndex:i-1];
        //printf("entered row4 loop %d\n", i);
        //NSLog(@"%@\n",current.text);
        //NSLog(@"%@\n",next.text);
        if([current.text isEqualToString:@""]  && ![next.text isEqualToString:@""] ){
            //printf("entered null\n");
            current.text = next.text;
            next.text = @"";
            if(i<3)
                i=i+2;
            else
                i++;
            changed = 1;
        }
        else if([current.text isEqualToString: next.text] && ![next.text isEqualToString:@""] && combineThresh>i){
            //printf("entered equal\n");
            int value = [current.text intValue]*2;
            if(value > highest)
                highest = value;
            totalScore += value/2;
            current.text = [NSString stringWithFormat:@"%d", value];
            next.text = @"";
            combineThresh = i;
            i++;
            changed = 1;
            tiles -=1;
        }
    }
    if(changed == 1)
        [self addElement];
    if(changed == 0)
        [self checkFull];
}

@end
