//
//  outterView.h
//  2048
//
//  Created by Ryan Simpson on 2/20/17.
//  Copyright © 2017 Ryan Simpson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface outterView : UIView

@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *row1;
@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *row2;
@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *row3;
@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *row4;
@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *column1;
@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *column2;
@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *column3;
@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *column4;
@property (nonatomic,strong) IBOutletCollection(UILabel) NSArray *all;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *highScoreLabel;
@property (weak, nonatomic) IBOutlet UIView *gridView;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIView *arrowView;
@property (weak, nonatomic) IBOutlet UILabel *label11;
@property (weak, nonatomic) IBOutlet UILabel *label12;
@property (weak, nonatomic) IBOutlet UILabel *label13;
@property (weak, nonatomic) IBOutlet UILabel *label14;
@property (weak, nonatomic) IBOutlet UILabel *label21;
@property (weak, nonatomic) IBOutlet UILabel *label22;
@property (weak, nonatomic) IBOutlet UILabel *label23;
@property (weak, nonatomic) IBOutlet UILabel *label24;
@property (weak, nonatomic) IBOutlet UILabel *label31;
@property (weak, nonatomic) IBOutlet UILabel *label32;
@property (weak, nonatomic) IBOutlet UILabel *label33;
@property (weak, nonatomic) IBOutlet UILabel *label34;
@property (weak, nonatomic) IBOutlet UILabel *label41;
@property (weak, nonatomic) IBOutlet UILabel *label42;
@property (weak, nonatomic) IBOutlet UILabel *label43;
@property (weak, nonatomic) IBOutlet UILabel *label44;

-(void)addElement;
-(void)colorTiles;
-(void)colorBackground;
-(void)updateScore;
-(void)checkFull;

- (UIColor*)color0;
- (UIColor*)color2;
- (UIColor*)color4;
- (UIColor*)color8;
- (UIColor*)color16;
- (UIColor*)color32;
- (UIColor*)color64;
- (UIColor*)color128;
- (UIColor*)color256;
- (UIColor*)color512;
- (UIColor*)color1024;
- (UIColor*)color2048;
- (UIColor*)color4096;


@end
